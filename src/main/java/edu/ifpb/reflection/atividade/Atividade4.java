package edu.ifpb.reflection.atividade;



import edu.ifpb.reflection.Pessoa;
import edu.ifpb.reflection.atividade.*;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ricardo Job
 */
public class Atividade4 {
    public static void main(String[] args) {
        Pessoa p = new Pessoa();
        p.setNome("Kiko");
        System.out.println(p.getNome());
        getInfo(p);
                
    }
    
    public static void getInfo(Object o){
        
        try {
            Field f = o.getClass().getDeclaredField("nome");
            f.setAccessible(true);
            f.set(o, "Chaves");
            System.out.println(f.get(o));
            
        } catch (NoSuchFieldException ex) {
            Logger.getLogger(Atividade4.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(Atividade4.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(Atividade4.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Atividade4.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
