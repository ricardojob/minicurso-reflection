package edu.ifpb.reflection.atividade;

import edu.ifpb.reflection.Modelo;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ricardo Job
 */
public class Atividade9 {

    public static void main(String[] args) {
        Class c;
        try {
            c = Class.forName("edu.ifpb.reflection.Pessoa");
            getInfo(c);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Atividade9.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void getInfo(Class classe) {

        try {

//            Method[] methods = classe.getDeclaredMethods();
//            for (Method metodo : methods) {
            Method metodo = classe.getDeclaredMethod("toString", null);
            Annotation[] anotacoes = metodo.getDeclaredAnnotations();
            for (Annotation anotacao : anotacoes) {
                System.out.println(anotacao.annotationType().getSimpleName());
            }
//            }

        } catch (NoSuchMethodException ex) {
            Logger.getLogger(Atividade9.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(Atividade9.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
