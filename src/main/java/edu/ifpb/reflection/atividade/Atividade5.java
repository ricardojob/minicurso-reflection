package edu.ifpb.reflection.atividade;

import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ricardo Job
 */
public class Atividade5 {
    public static void main(String[] args) {
      Class c;
        try {
            c = Class.forName("edu.ifpb.reflection.Pessoa");
            getInfo(c); 
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Atividade5.class.getName()).log(Level.SEVERE, null, ex);
        }            
    }
    
    public static void getInfo(Class classe){
        Field[] f = classe.getDeclaredFields();
        
        System.out.println(f.length);
        for (Field atributo : f) {
            System.out.println(atributo.getName()+" "+atributo.getModifiers());
        }               
    }
}
