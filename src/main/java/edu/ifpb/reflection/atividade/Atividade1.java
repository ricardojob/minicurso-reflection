/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.reflection.atividade;

import edu.ifpb.reflection.Pessoa;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ricardo Job
 */
public class Atividade1 {
    public static void main(String[] args) {
//        Pessoa p = new Pessoa();
        
//        Class c = p.getClass();
        Class c;
        try {
            c = Class.forName("edu.ifpb.reflection.Endereco");
            getInfo(c); 
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Atividade1.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }
    
    public static void getInfo(Class classe){
        System.out.println(classe.getName());
        System.out.println(classe.getSimpleName());
    }
}
