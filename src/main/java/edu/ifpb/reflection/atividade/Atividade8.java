package edu.ifpb.reflection.atividade;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ricardo Job
 */
public class Atividade8 {
    public static void main(String[] args) {

        Class c;
        try {
            c = Class.forName("edu.ifpb.reflection.Pessoa");
            getInfo(c); 
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Atividade8.class.getName()).log(Level.SEVERE, null, ex);
        }            
    }
    
    public static void getInfo(Class classe){
        Constructor[] c = classe.getConstructors();
        
        
        for (Constructor construtor : c) {
            System.out.print(construtor.getName()+"( ");
            Class[] parametros = construtor.getParameterTypes();
            for (Class parametro : parametros) {
                System.out.print(parametro.getSimpleName()+" ");
            }
            System.out.println(")");
        }
    }
}
