package edu.ifpb.reflection.atividade;

import edu.ifpb.reflection.Modelo;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ricardo Job
 */
public class Atividade11 {

    public static void main(String[] args) {
        Class c;
        try {
            c = Class.forName("edu.ifpb.reflection.CadastraPessoa");
            getInfo(c);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Atividade11.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void getInfo(Class classe) {
        Modelo modelo = (Modelo) classe.getAnnotation(Modelo.class);
        System.out.println(modelo.classe());
    }
}
