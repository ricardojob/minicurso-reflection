package edu.ifpb.reflection.atividade;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ricardo Job
 */
public class Atividade7 {
    public static void main(String[] args) {

        Class c;
        try {
            c = Class.forName("edu.ifpb.reflection.Pessoa");
            getInfo(c); 
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Atividade7.class.getName()).log(Level.SEVERE, null, ex);
        }            
    }
    
    public static void getInfo(Class classe){
        Method[] m = classe.getDeclaredMethods();
        
        for (Method metodo : m) {
            System.out.println(metodo.getName() + " " 
                    +metodo.getModifiers());
        }
    }
}
