/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.reflection;

import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ricardo Job
 */
public class ExportaXML {

    public static void main(String[] args) {
        Pessoa p = new Pessoa(1, "Chaves", "job@fsfdsd");

        Documento documento = new Documento("12121");

        Endereco e = new Endereco(3, "Rua do Chaves", "bairro", "Vida do Chaves");
        p.setEndereco(e);
        p.setDocumento(documento);
        
        try {
            lerObjeto(p);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ExportaXML.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void lerObjeto(Object obj) throws IllegalAccessException {
        String classe = obj.getClass().getSimpleName().toLowerCase();
        escrever("<" + classe + ">");
        Field[] campos = obj.getClass().getDeclaredFields();
        for (Field field : campos) {
            field.setAccessible(true);
            if (atributoSimples(field)) {
                ler(field, obj);
            } else {
                Object campoInterno = field.get(obj);
                lerObjeto(campoInterno);
            }
        }
        escrever("</" + classe + ">");
    }

    public static void ler(Field campo, Object obj) throws IllegalArgumentException, IllegalAccessException {
        escrever("<" + campo.getName() + "> " + campo.get(obj) + "</" + campo.getName() + ">");
    }

    public static void escrever(String linha) {
        System.out.println(linha);
    }

    private static boolean atributoSimples(Field field) {
        return field.getType().isPrimitive() || field.getType().getSimpleName().equals("String");
    }
}
