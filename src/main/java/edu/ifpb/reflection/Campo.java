package edu.ifpb.reflection;

import java.lang.annotation.Target;
import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;
import java.lang.annotation.Retention;

/**
 *
 * @author Ricardo Job
 */
@Target({TYPE, METHOD, PARAMETER, FIELD})
@Retention(RUNTIME)
public @interface Campo {

    public String nome() default "";

    public Class tipo() default String.class;
}
