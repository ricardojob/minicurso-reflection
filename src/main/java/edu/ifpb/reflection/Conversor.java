package edu.ifpb.reflection;

import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;

/**
 *
 * @author Ricardo Job
 */
public class Conversor {

    public Object convert(Class classe, CadastraPessoa tela) {

        try {
            Object object = classe.newInstance();
            Field[] campos = tela.getClass().getDeclaredFields();
            for (Field campo : campos) {
                Campo valor = campo.getAnnotation(Campo.class);
                if (valor != null) {
                    campo.setAccessible(true);
                    JTextField valorCampo = (JTextField) campo.get(tela);

                    Field f = classe.getDeclaredField(valor.nome());
                    if (f != null) {
                        f.setAccessible(true);
                        f.set(object, valorCampo.getText());
                    }
                }
            }
            return object;

        } catch (IllegalArgumentException ex) {
            Logger.getLogger(Conversor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Conversor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(Conversor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchFieldException ex) {
            //Logger.getLogger(Conversor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(Conversor.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

}
