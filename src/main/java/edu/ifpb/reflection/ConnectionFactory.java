package edu.ifpb.reflection;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ResourceBundle;

public class ConnectionFactory {

    private String url;
    private String password;
    private String user;
    private String driver;
    private static ConnectionFactory instance = null;

    private ConnectionFactory(String resouceName) {
        ResourceBundle bounder = ResourceBundle.getBundle(resouceName);
        this.url = bounder.getString("url");
        this.password = bounder.getString("password");
        this.user = bounder.getString("user");
        this.driver = bounder.getString("driver");
    }

    public static ConnectionFactory getInstance(String resouceName) {
        if (instance == null) {
            instance = new ConnectionFactory(resouceName);
        }
        return instance;
    }

    public Connection getConnection() throws SQLException {
        try {
            Class.forName(this.driver);
            return DriverManager.getConnection(this.url, this.user, this.password);
        } catch (ClassNotFoundException ex) {
            throw new SQLException(ex);
        }
    }
}

//CREATE TABLE pessoa
//(
//  id integer NOT NULL,
//  nome character varying(30) NOT NULL,
//  email character varying(50),
//  CONSTRAINT pessoa_pkey PRIMARY KEY (id)
//)
//WITHOUT OIDS;
//ALTER TABLE pessoa OWNER TO postgres;




