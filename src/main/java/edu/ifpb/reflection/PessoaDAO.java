package edu.ifpb.reflection;

import java.sql.*;
import java.util.*;

public class PessoaDAO {

    private Connection conexao;
    private PreparedStatement statment;

    public PessoaDAO() throws SQLException {
        abrir();
    }

    public void salvar(Pessoa pessoa) throws SQLException {
        try {
            abrir();
            String comando = "insert into pessoa (nome, email) values (?, ?)";
            this.statment = this.conexao.prepareStatement(comando);
            this.statment.setString(1, pessoa.getNome());
            this.statment.setString(2, pessoa.getEmail());
            this.statment.executeUpdate();
        } finally {
            this.liberar();
        }
    }

    public void salvarComEndereco(Pessoa pessoa) throws SQLException {
        try {
            abrir();
            String comando = "insert into pessoa (nome, email) values (?, ?)";
            this.statment = this.conexao.prepareStatement(comando, Statement.RETURN_GENERATED_KEYS);
            this.statment.setString(1, pessoa.getNome());
            this.statment.setString(2, pessoa.getEmail());

            System.out.println(this.statment.executeUpdate());
            //ResultSet rs = pstmt.getGeneratedKeys(); // will return the ID in ID_COLUMN

            ResultSet resultado = this.statment.getGeneratedKeys();
            if (resultado.next()) {
                System.out.println(resultado.getMetaData().getColumnName(1));
                int id = resultado.getInt("1");
                System.out.println("fsdfa: " + id);
                System.out.println(resultado);
                comando = "insert into endereco (bairro, cidade, logradouro, codigo_pessoa) values (?, ?,?,?)";
                this.statment = this.conexao.prepareStatement(comando);
                this.statment.setString(1, pessoa.getEndereco().getBairro());
                this.statment.setString(2, pessoa.getEndereco().getCidade());
                this.statment.setString(3, pessoa.getEndereco().getLogradouro());
                this.statment.setInt(4, id);
                this.statment.executeUpdate();
            } else {
                throw new SQLException("Não foi possivel recuperar o ID da Pessoa");
            }

        } finally {
            this.liberar();
        }
    }

    public List<Pessoa> listar() throws SQLException {
        String comando = "Select * from pessoa";
        return listar(comando);
    }

    private Endereco localizaEndereco(int id) throws SQLException {
        String comando = "select * from endereco where codigo_pessoa=?";
        PreparedStatement statmentLocal = this.conexao.prepareStatement(comando);
        statmentLocal.setInt(1, id);
        ResultSet rs = statmentLocal.executeQuery();
        Endereco endereco = new Endereco();
        while (rs.next()) {
            int codigo = rs.getInt("codigo");
            String bairro = rs.getString("bairro");
            String cidade = rs.getString("cidade");
            String logradouro = rs.getString("logradouro");
            endereco.setLogradouro(logradouro);
            endereco.setCidade(cidade);
            endereco.setBairro(bairro);
            endereco.setCodigo(codigo);
        }
        rs.close();
        return endereco;
    }

    @SuppressWarnings("unchecked")
    public List<Pessoa> listar(String comando) throws SQLException {
        List pessoas = new ArrayList<Pessoa>();
        ResultSet rs;
        try {
            abrir();
            // String comando = "select id, nome, email from pessoa order by nome";
            this.statment = this.conexao.prepareStatement(comando);
            rs = this.statment.executeQuery();
            while (rs.next()) {
                Integer id = rs.getInt("id");
                String nome = rs.getString("nome");
                String email = rs.getString("email");
                Pessoa pessoa = new Pessoa();
                pessoa.setId(id);
                pessoa.setNome(nome);
                pessoa.setEmail(email);
                pessoa.setEndereco(localizaEndereco(id));
                pessoas.add(pessoa);
            }
            rs.close();
        } finally {
            this.liberar();
            return pessoas;
        }
    }

    private void abrir() throws SQLException {
        if (this.conexao == null || this.conexao.isClosed()) {
            this.conexao = ConnectionFactory.getInstance("jdbc").getConnection();
            this.statment = null;
        }
    }

    private void liberar() throws SQLException {
        if (this.statment != null) {
            this.statment.close();
            this.statment = null;
        }
        if (this.conexao != null) {
            this.conexao.close();
            this.conexao = null;
        }
    }
}
